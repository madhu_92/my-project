package tests;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class checkOddEven {

	public static void main(String[] args) throws Exception, IOException {

		int inputNum;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the number you want to check for odd or even");

		inputNum = Integer.parseInt(br.readLine());
		if (inputNum != 0) {
			if (inputNum % 2 != 0) {
				System.out.println("Entered number is odd");
			} else
				System.out.println("Entered number is even");
		}
		else System.out.println("Enter number other than zero");

	}
}
